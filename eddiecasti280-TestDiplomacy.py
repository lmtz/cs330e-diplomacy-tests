#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
	# ----
	# diplomacy_solve()
	# ----	
	def test_diplomacy_solve_1(self):
		r = StringIO("")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), "")

	def test_diplomacy_solve_2(self):
		r = StringIO("A Madrid Hold\nC Seattle Move Madrid\nD OklahomaCity Support A\nB SanAntonio Support C\nE Washington Hold\nF Tampa Hold\nG Santiago Support F\nH Paris Move Tampa")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), "A [dead]\nB SanAntonio\nC [dead]\nD OklahomaCity\nE Washington\nF Tampa\nG Santiago\nH [dead]")
	
	def test_diplomacy_solve_3(self):
		r = StringIO("A Madrid Move Tokyo\nB Tokyo Hold\nC Seattle Support A\nD OklahomaCity Support B\nE Paris Support F\nF Madison Hold\nG Austin Move Paris")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Seattle\nD OklahomaCity\nE [dead]\nF Madison\nG [dead]")

# ----
# main
# ----


if __name__ == "__main__":
    main()

#""" #pragma: no cover